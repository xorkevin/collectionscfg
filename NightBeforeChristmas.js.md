# NightBeforeChristmas.js

'Twas the night before Launch Day, and all thro' the house  
Not a keyboard was stirring, no trackpad or mouse;  
The branches committed, every package declared,  
All sources deployed and the README prepared;  
The code fully linted, the test suites created,  
Dependencies synced and the Shrinkwrap updated;  
The work all complete, I slid into a nap,  
And while I was dozing, I dreamt of the app —  
When, there from my phone there arose such a clatter,  
I sprang from the bed to see what was the matter;  
"Support" said the voice, and before we had spoken,  
I knew why they’d called me — the app was quite broken;  
I frantically probed at the cause of the crash  
("Did they refresh the page? Did they empty the cache?")  
Growing more desperate and racked by confusion,  
I looked to the skies as I craved resolution;  
Then, what to my wondering eyes should appear,  
But a miniature sleigh, and eight tiny reindeer;  
With no moment to lose, they swung into action,  
As two-at-a-time they pursued the infraction;  
With Dasher and Dancer examining traces,  
Comet and Cupid set to balancing braces;  
Prancer and Vixen combed source code for nits,  
While Donner and Blitzen bisected commits;  
Before very long they’d discovered the glitch  
(I'd forgotten a `break` and so fell through the `switch`)  
Then, working together, a patch was submitted,  
The code was accepted, solution commited;  
Before I could thank them, the reindeer were gone,  
(all that remained were their tracks on the lawn);  
But I heard them exclaim, as they flew out of sight —  
"You can whine about Crockford, but sometimes he’s right!"  
